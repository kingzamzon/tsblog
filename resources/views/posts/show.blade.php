@extends('layouts.app')  

@section('content')
<section>
        <div class="container">
            <div class="row">
                
                <div class="col-md-12 col-lg-8">
                    @if($post )
                    @include('inc.messages')
                    <img src="/images/{{$post->cover_image}}" alt="{{$post->title}}">
                    <h3 class="mt-30"><b>{{$post->title}}</b></h3>
                    <ul class="list-li-mr-20 mtb-15">
                        <li>by <a href="#"><b>{{$post->user->name}}</b></a> {{date('M d, Y', strtotime($post->created_at))}}</li>
                        <li><i class="color-primary mr-5 font-12 ion-chatbubbles"></i>{{count($post->comment)}}</li>
                    </ul>
                    
                    <p class="mtb-15">
                        {!!$post->body!!}
                    </p>
                        
                    <div class="float-left-right text-center mt-40 mt-sm-20">
                
                        <ul class="mb-30 list-li-mt-10 list-li-mr-5 list-a-plr-15 list-a-ptb-7 list-a-bg-grey list-a-br-2 list-a-hvr-primary ">
                            <li><a href="/category/{{$post->category}}">{{strtoupper($post->category)}}</a></li>
                        </ul>
                        <ul class="mb-30 list-a-bg-grey list-a-hw-radial-35 list-a-hvr-primary list-li-ml-5">
                            <li class="mr-10 ml-0">Share</li>
                            <li><a href="#"><div data-network="facebook" class="st-custom-button ion-social-facebook"></div></a></li>
                            <li><a href="#"><div data-network="twitter" class="st-custom-button ion-social-twitter"></div></a></li>
                            <li><a href="#"><div data-network="gmail" class="st-custom-button ion-social-google"></div></a></li>
                            <li><a href="#"><div data-network="whatsapp" class="st-custom-button ion-social-whatsapp"></div></a></li>
                        </ul>
                        
                    </div><!-- float-left-right -->
                     @endif
                    <div class="brdr-ash-1 opacty-5"></div>
                    
                    <h4 class="p-title mt-50"><b>YOU MAY ALSO LIKE</b></h4>
                    <div class="row">
                        @if(count($randomposts) > 0)
                        @foreach ($randomposts as $random)
                        <div class="col-sm-6">
                            <img src="/images/{{$random->cover_image}}" class="col-sm-5">
                            <h4 class="pt-20"><a href="/posts/{{$random->id}}"><b>{{$random->title}}: <br/>
                                {{$random->category}}</b></a></h4>
                            <ul class="list-li-mr-20 pt-10 mb-30">
                                <li class="color-lite-black">by <a href="#" class="color-black"><b>{{$random->user->name}},</b></a>
                                {{date('M d, Y', strtotime($random->created_at))}}</li>
                                <li><i class="color-primary mr-5 font-12 ion-chatbubbles"></i>
                                    {{count($random->comment)}}</li>
                            </ul>
                        </div><!-- col-sm-6 -->
                        @endforeach
                        @endif
                    </div><!-- row -->

                    <h4 class="p-title mt-20"><b>LEAVE A COMMENT</b></h4>
                    <div id="comment">
                         <form role="form" method="POST" action="{{action('Post\CommentController@store')}}" class="form-block form-plr-15 form-h-45 form-mb-20 form-brdr-lite-white mb-md-50">
                        {{csrf_field()}}
                        <input type="hidden" name="post_id" value="{!!$post->id!!}">
                        <input type="text" name="name" placeholder="Your Name*:">
                        <input type="text" name="email" placeholder="Your Email*:">
                        <textarea class="ptb-10" placeholder="Your Comment" name ="comment"></textarea>
                        <button class="btn-fill-primary plr-30"  type="submit"><b>LEAVE A COMMENT</b></button>
                        </form>
                    </div>
                  
                    
                    <h4 class="p-title mt-20"><b>{{count($post->comment)}} COMMENTS</b></h4>
                   
                    @if(count($post->comment) > 0)
                        @foreach ($post->comment as $comment)
                    <div class="sided-70 mb-40">
                    
                        <div class="s-left rounded">
                            <img src="/images/profile-3-120x120.jpg" alt="">
                        </div><!-- s-left -->
                        
                        <div class="s-right ml-100 ml-xs-85">
                            <h5><b>{{$comment->name}}, </b> <span class="font-8 color-ash">{{date('M d, Y', strtotime($comment->created_at))}}</span></h5>
                            <p class="mtb-15">{{$comment->comment}}</p>

                            <form role="form" method="POST" action="{{action('Post\CommentController@show', ['id' => $comment->id])}}">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="GET">
                        <button type="submit" class="btn-brdr-grey btn-b-sm plr-15 mr-10 mt-5"><b>{{$comment->favourite}} Like</b></button>
                        <a class="btn-brdr-grey btn-b-sm plr-15 mt-5" href="#comment"><b>REPLY</b></a>
                        </form>
                            
                        </div><!-- s-right -->
                        
                    </div><!-- sided-70 -->
                       @endforeach

                       @else 
                       <div class="sided-70 mb-40">
                        <h5>Be the first to comment</h5>
                       </div>
                    @endif
                    
                    
                   
                </div><!-- col-md-9 -->
                
                <div class="col-md-6 col-lg-4">
                        @include('inc.sidebar')
                </div><!-- col-md-3 -->
                
            </div><!-- row -->
            
        </div><!-- container -->
    </section>
@endsection


