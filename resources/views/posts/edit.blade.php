@extends('layouts.app')  

@section('content')
<div class="container">

<div class="row">
        <div class="col-sm-2">
            <h3 class="mt-10">Create Post</h3>
            <p>Hava a nice article?</p>  
            <b class="mt-10"><a href="/home">Home</a>  </b>
        </div>
        <div class="col-sm-10" content-section>
            <div class="container mt-20">
                 @include('inc.messages')
            <form role="form" class="mt-10" method="POST" action="{{action('Post\PostController@update', ['id' => $post->id])}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="PUT">
                <div class="form-group">
                    <label for="title">Title:</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{$post->title}}">
                </div>
                <div class="form-group">
                    <label for="body">Body:</label>
                    <textarea id='editor' class="form-control" rows="5"  name="body" placeholder="Enter Body">
                        {!!$post->body!!}
                    </textarea>
                </div>
                <div class="form-group">
                      <label for="type">Select Type(select one):</label>
                      <select class="form-control" id="type" name="category">
                        <option>World</option>
                        <option>Politics</option>
                        <option>Sport</option>
                        <option>Business</option>
                        <option>Technology</option>
                        <option>Health</option>
                        <option>Fashion</option>
                        <option>Religion</option>
                      </select>
                </div>
                <div class="form-group">
                        <label for="body">Cover Image:</label>
                        <input type="file" name="cover_image" id="">
                </div>
                <button type="submit" class="btn-fill-primary plr-30 mb-20">
                    <b>SUBMIT</b>
                </button>
            </form>
            </div>
           
        </div>
</div>
</div>
@endsection