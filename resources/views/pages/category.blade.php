@extends('layouts.app')  

@section('title', ucfirst($type))

@section('content')

	<section class="ptb-0">
		<div class="mb-30 brdr-ash-1 opacty-5"></div>
		<div class="container">
			<a class="mt-10" href="/"><i class="mr-5 ion-ios-home"></i>Home<i class="mlr-10 ion-chevron-right"></i></a>
			<a class="mt-10 color-ash" href="#">{{ucfirst($type)}} Archive</a>
		</div><!-- container -->
	</section>

<section>
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-8">
					<h4 class="p-title"><b>{{strtoupper($type)}}</b></h4>
					<div class="row">
						@if(count($posts) > 0)
					        @foreach ($posts as $post)
					           
					        <div class="col-sm-6">
					   			<img src="/images/{{$post->cover_image}}" alt="">
								<h4 class="pt-20">
									<a href="/posts/{{$post->slug}}">
										<b>{{$post->title}}</b>
									</a>
								</h4>
								<ul class="list-li-mr-20 pt-10 mb-30">
									<li class="color-lite-black">
										by <a href="#" class="color-black"><b>{{$post->user->name}},</b></a>
										{{date('M d, Y', strtotime($post->created_at))}}</li>
									<li>
										<i class="color-primary mr-5 font-12 ion-chatbubbles"></i>{{count($post->comment)}}
									</li>
									<p>{!!str_limit($post->body, 50)!!}</p>
								</ul>

							</div><!-- col-sm-6 -->
					         @endforeach
					            {{$posts->links()}}
						    @else
						    	<div class="col-sm-6">
						        <p>No Post Found </p>
						    	</div>
						    @endif
					</div><!-- row -->
					
					<a class="dplay-block btn-brdr-primary mt-20 mb-md-50" href="#"><b>LOAD MORE</b></a>
				</div><!-- col-md-9 -->
				
				
				  <div class="col-md-6 col-lg-4">
                     @include('inc.sidebar')
				  </div><!-- row -->
		</div><!-- container -->
	</section>
@endsection