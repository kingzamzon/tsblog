@extends('layouts.app')  

@section('content')
    @include('inc.banner')
    <section>
        <div class="container">
            <div class="row">
            
                <div class="col-md-12 col-lg-8">
                    <h4 class="p-title"><b>RECENT</b></h4>
                    <div class="row">
                       @if(count($groupedposts) > 0)
                       @foreach ($groupedposts as $recentpost)
                        <div class="col-sm-6">
                            <img src="/images/{{$recentpost->cover_image}}" alt="" height="200rem"  style="width:20rem;">
                            <h4 class="pt-20"><a href="/posts/{{$recentpost->slug}}"><b>{{$recentpost->title}}: <br/>{{$recentpost->category}}</b></a></h4>
                            <ul class="list-li-mr-20 pt-10 pb-20">
                                <li class="color-lite-black">by <a href="#" class="color-black"><b>{{$recentpost->user->name}},</b></a>
                                {{date('M d, Y', strtotime($recentpost->created_at))}}</li>
                                <li><i class="color-primary mr-5 font-12 ion-chatbubbles"></i><b>{{count($recentpost->comment)}}</b></li>
                            </ul>
                        </div><!-- col-sm-6 -->
                         @endforeach
                            <div class="col-sm-12">
                                <p class="mt-20 mb-md-50 ">
                                    <b>{{$groupedposts->links()}}</b>
                                </p>
                            </div>
                            
                        @else 
                        <div class="col-sm-12">
                          No post Found
                        </div><!-- col-sm-12 -->
                        @endif
                    </div><!-- row -->

           
                
                </div><!-- col-md-9 -->
                <div class="col-md-6 col-lg-4">
                  @include('inc.sidebar')
                </div><!-- col-md-3 -->
                
            </div><!-- row -->
        </div><!-- container -->
    </section>
@endsection


