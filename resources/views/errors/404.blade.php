@extends('layouts.app')  

@section('content')
<section>
	 <div class="container">
		<h2 class="pl-10">
			{{ $exception->getMessage() }}
		</h2>
	</div>
</section>
@endsection