@if(count($recentposts) > 0)
<div class="container">
        <div class="h-600x h-sm-auto">
            <div class="h-2-3 h-sm-auto oflow-hidden">
                @if(count($recentposts) > 0)
                <div class="pb-5 pr-5 pr-sm-0 float-left float-sm-none w-2-3 w-sm-100 h-100 h-sm-300x">
                    <a class="pos-relative h-100 dplay-block" href="/posts/{{$recentposts[0]->slug}}">
                    
                        <div class="img-bg  bg-grad-layer-6" style='background: url(/images/{{$recentposts[0]->cover_image}}) no-repeat center; background-size: cover;'></div>
                        
                        <div class="abs-blr color-white p-20 bg-sm-color-7">
                            <h3 class="mb-15 mb-sm-5 font-sm-13"><b>{{$recentposts[0]->title}}</b></h3>
                            <ul class="list-li-mr-20">
                                <li>by <span class="color-primary"><b>{{$recentposts[0]->user->name}}</b></span> {{date('M d, Y', strtotime($recentposts[0]->created_at))}}</li>
                                <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i>{{count($recentposts[0]->comment)}}</li>
                            </ul>
                        </div><!--abs-blr -->
                    </a><!-- pos-relative -->
                </div><!-- w-1-3 -->
                @endif
                
                <div class="float-left float-sm-none w-1-3 w-sm-100 h-100 h-sm-600x">
                
                    @if(count($recentposts) > 1)
                    <div class="pl-5 pb-5 pl-sm-0 ptb-sm-5 pos-relative h-50">
                        <a class="pos-relative h-100 dplay-block" href="/posts/{{$recentposts[1]->slug}}">
                        
                            <div class="img-bg bg-grad-layer-6" style='background: url(/images/{{$recentposts[1]->cover_image}}) no-repeat center; background-size: cover;'></div>
                            
                            <div class="abs-blr color-white p-20 bg-sm-color-7">
                                <h4 class="mb-10 mb-sm-5"><b>{{$recentposts[1]->title}}</b></h4>
                                <ul class="list-li-mr-20">
                                    <li>{{date('M d, Y', strtotime($recentposts[1]->created_at))}}</li>
                                    <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i>{{count($recentposts[1]->comment)}}</li>
                                </ul>
                            </div><!--abs-blr -->
                        </a><!-- pos-relative -->
                    </div><!-- w-1-3 -->
                    @endif

                    @if(count($recentposts) > 2)
                    <div class="pl-5 ptb-5 pl-sm-0 pos-relative h-50">
                        <a class="pos-relative h-100 dplay-block" href="/posts/{{$recentposts[2]->slug}}">
                        
                            <div class="img-bg bg-grad-layer-6" style='background: url(/images/{{$recentposts[2]->cover_image}}) no-repeat center; background-size: cover;'></div>
                            
                            <div class="abs-blr color-white p-20 bg-sm-color-7">
                                <h4 class="mb-10 mb-sm-5"><b>{{$recentposts[2]->title}}</b></h4>
                                <ul class="list-li-mr-20">
                                    <li>{{date('M d, Y', strtotime($recentposts[2]->created_at))}}</li>
                                    <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i>{{count($recentposts[2]->comment)}}</li>
                                </ul>
                            </div><!--abs-blr -->
                        </a><!-- pos-relative -->
                    </div><!-- w-1-3 -->
                    @endif

                </div><!-- float-left -->

            </div><!-- h-2-3 -->
            
            <div class="h-1-3 oflow-hidden">
                @if(count($recentposts) > 3)
                <div class="pr-5 pr-sm-0 pt-5 float-left float-sm-none pos-relative w-1-3 w-sm-100 h-100 h-sm-300x">
                    <a class="pos-relative h-100 dplay-block" href="/posts/{{$recentposts[3]->slug}}">
                    
                        <div class="img-bg bg-grad-layer-6" style='background: url(/images/{{$recentposts[3]->cover_image}}) no-repeat center; background-size: cover;'></div>
                        
                        <div class="abs-blr color-white p-20 bg-sm-color-7">
                            <h4 class="mb-10 mb-sm-5"><b>{{$recentposts[3]->title}}</b></h4>
                            <ul class="list-li-mr-20">
                                <li>{{date('M d, Y', strtotime($recentposts[3]->created_at))}}</li>
                                <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i>{{count($recentposts[3]->comment)}}</li>
                            </ul>
                        </div><!--abs-blr -->
                    </a><!-- pos-relative -->
                </div><!-- w-1-3 -->
                @endif

                @if(count($recentposts) > 4)
                <div class="plr-5 plr-sm-0 pt-5 pt-sm-10 float-left float-sm-none pos-relative w-1-3 w-sm-100 h-100 h-sm-300x">
                    <a class="pos-relative h-100 dplay-block" href="/posts/{{$recentposts[4]->slug}}">
                    
                        <div class="img-bg bg-grad-layer-6" style='background: url(/images/{{$recentposts[4]->cover_image}}) no-repeat center; background-size: cover;'></div>
                        
                        <div class="abs-blr color-white p-20 bg-sm-color-7">
                            <h4 class="mb-10 mb-sm-5"><b>{{$recentposts[4]->title}}</b></h4>
                            <ul class="list-li-mr-20">
                                <li>{{date('M d, Y', strtotime($recentposts[4]->created_at))}}</li>
                                <li><i class="color-primary mr-5 font-12 ion-ios-bolt"></i>{{count($recentposts[4]->comment)}}</li>
                            </ul>
                        </div><!--abs-blr -->
                    </a><!-- pos-relative -->
                </div><!-- w-1-3 -->
                @endif

                @if(count($recentposts) > 5)
                <div class="pl-5 pl-sm-0 pt-5 pt-sm-10 float-left float-sm-none pos-relative w-1-3 w-sm-100 h-100 h-sm-300x">
                    <a class="pos-relative h-100 dplay-block" href="/posts/{{$recentposts[5]->slug}}">
                    
                        <div class="img-bg bg-grad-layer-6" style='background: url(/images/{{$recentposts[5]->cover_image}}) no-repeat center; background-size: cover;'></div>
                        
                        <div class="abs-blr color-white p-20 bg-sm-color-7">
                            <h4 class="mb-10 mb-sm-5"><b>{{$recentposts[5]->title}}</b></h4>
                            <ul class="list-li-mr-20">
                                <li>{{date('M d, Y', strtotime($recentposts[5]->created_at))}}</li>
                                <li><i class="color-primary mr-5 font-12 ion-chatbubbles"></i>{{count($recentposts[5]->comment)}}</li>
                            </ul>
                        </div><!--abs-blr -->
                    </a><!-- pos-relative -->
                </div><!-- w-1-3 -->
                @endif
            </div><!-- h-2-3 -->

        </div><!-- h-100vh -->
    </div><!-- container -->
@else 
    <div class="container">
              <p>Post Something to get started</p>
    </div>
  
@endif
