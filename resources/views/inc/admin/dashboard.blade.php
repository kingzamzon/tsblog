@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif
@include('inc.messages')
<div class="clearfix container-fluid bg-success mt-2 mb-2">
  <span class="float-left">
     <h4>DashBoard</h4>
  </span>
</div>

<div class="card-columns card-deck">
    @if(Auth::user()->isAdmin == 1)
    <div class="card " style="background-color: #20A8D8;color:white">
        <div class="card-body text-center">
            <p class="card-text">
                <h1>{{count($posts)}}</h1>
                Posts
            </p>
        </div>
    </div> 
    <div class="card " style="background-color: #63C2DE;color:white">
        <div class="card-body text-center">
            <p class="card-text">
                <h1>{{count($unapprovedcomments)}}</h1>
                UnApprove Comments
            </p>
        </div>
    </div> 
    <div class="card " style="background-color: #FFC107;color:white">
        <div class="card-body text-center">
            <p class="card-text">
                <h1>{{count($subscribers)}}</h1>
                Subscribers
            </p>
        </div>
    </div> 
    @else 
        <div class="card col-sm-3" style="background-color: #20A8D8;color:white">
            <div class="card-body text-center">
                <p class="card-text">
                    <h1>{{count($posts->where('user_id', Auth::user()->id))}}</h1>
                    Posts
                </p>
            </div>
        </div>
    @endif
</div>
<a href="{{ route('posts.create') }}" class="btn btn-success btn-sm mb-4">Create new post</a>

      <div class="alert alert-success">
            <strong>Welcome!</strong>  <a href="#" class="alert-link"> {{ Auth::user()->name }} </a>.
          </div>
@if(Auth::user()->isAdmin !== 1)

    @include('inc.admin.posts')
@endif