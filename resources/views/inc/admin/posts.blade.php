<div class="clearfix container-fluid bg-success mt-2 mb-10">
    <span class="float-left">
        <h4>Posts</h4>
    </span>
    <span class="float-right pt-1" style="list-style: none;display:flex">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item active">Posts</li>
    </span>
</div>
 <a href="{{ route('posts.create') }}" class="btn btn-success btn-sm">Create new post</a>
                        <br/><br/>
  @if(count($posts) > 0)

  <table class="table  table-bordered table-hover table-danger">
      <thead>
          <tr>
              <th>Title</th>
              <th>Views</th>
              <th></th>
              <th></th>
          </tr>
      </thead>

      <tbody>
        @if(Auth::user()->isAdmin == 1)
          @foreach ($posts as $post)
            <tr>
              <td> {{str_limit($post->title, 20)}}</td>
              <td> {{$post->views}}</td>
              <td>
                <a href="/posts/{{$post->slug}}/edit" class="btn btn-info btn-sm">Update</a>
              <td>
                <form role="form" method="POST" action="{{action('Post\PostController@destroy', ['id' => $post->id])}}">
                  {{csrf_field()}}
                  <input type="hidden" name="_method" value="DELETE">
                  <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                </form>
              </td>
            </tr>
          @endforeach
            {{$posts->links()}}
        @else 
            @foreach ($posts->where('user_id', Auth::user()->id) as $post)
            <tr>
              <td> {{str_limit($post->title, 20)}}</td>
              <td> {{$post->views}}</td>
              <td><button type="button" class="btn btn-info btn-sm">Update</button></td>
              <td>
                <form role="form" method="POST" action="{{action('Post\PostController@destroy', ['id' => $post->id])}}">
                  {{csrf_field()}}
                  <input type="hidden" name="_method" value="DELETE">
                  <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                </form>
              </td>
            </tr>
          @endforeach
            {{$posts->links()}}
        @endif
      </tbody>
  </table>
  @else
    <div>
      <td>No Post Found </td>
    </div>
  @endif
