<div class="clearfix container-fluid bg-success mt-2 mb-10">
    <span class="float-left">
        <h4>Comments</h4>
    </span>
    <span class="float-right pt-1" style="list-style: none;display:flex">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item active">Comments</li>
    </span>
</div>

 @if(count($unapprovedcomments->where("isApprove", 0)) > 0)
  <div class="row">
    @foreach ($unapprovedcomments->where("isApprove", 0) as $unapprovedcomment)
        <div class="media border p-3">
            <img src="/" alt="{{$unapprovedcomment->name}}" class="mr-3 mt-3 rounded-circle" style="width:60px;">
            <div class="media-body">
              <h4>{{$unapprovedcomment->name}} <small><i>Comment on {{$unapprovedcomment->post->title}}</i></small></h4>
              <p> - {{$unapprovedcomment->comment}}</p>
              <p>{{date('M d, Y', strtotime($unapprovedcomment->created_at))}}</p>
              <span>
                  <form role="form" method="POST" action="{{action('Post\CommentController@update', ['id' => $unapprovedcomment->id])}}">
                  {{csrf_field()}}
                  <input type="hidden" name="_method" value="PATCH">
                  <button type="submit" class="btn btn-info btn-sm">Approve</button>
                  </form>
              </span>
              <span>
                  <form role="form" method="POST" action="{{action('Post\CommentController@destroy', ['id' => $unapprovedcomment->id])}}">
              {{csrf_field()}}
              <input type="hidden" name="_method" value="DELETE">
              <button type="submit" class="btn btn-danger btn-sm">Delete</button>
            </form>
              </span>
              
            </div>
      </div>
    @endforeach
  </div>
  {{$posts->links()}}
  @else 
    <div>
        <div>
            <p>No New Comment</p>
        </div>
    </div>

@endif