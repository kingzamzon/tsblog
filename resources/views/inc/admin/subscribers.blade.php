<div class="clearfix container-fluid bg-success mt-2 mb-2">
    <span class="float-left">
        <h4>Subscribers</h4>
    </span>
    <span class="float-right pt-1" style="list-style: none;display:flex">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item active">Subscribers</li>
    </span>
</div>

@if(count($subscribers) > 0)
    <table class="table table-hover">
        <thead>
          <tr>
            <th>Email</th>
            <th>Date</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($subscribers as $subscriber)
            <tr>
              <td>  {{$subscriber->email}}</td>
              <td>  {{date('M d, Y', strtotime($subscriber->created_at))}}</td>
            </tr>
          @endforeach
          {{$posts->links()}}
          </tbody>
    </table>
@else 
    <div class="pl-10">
      <p>No Subscribers so far</p>
    </div>
@endif