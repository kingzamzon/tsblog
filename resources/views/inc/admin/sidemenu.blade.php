<aside class="col-sm-2" admin-menu>
    <!-- Nav tabs -->
    <ul class="nav flex-column" role="tablist">
        <li class="nav-item mt-20" id="list-group-item" >
            <a class="nav-link active" data-toggle="tab" href="#dashboard">
                <div class="clearfix">
                    <span class="float-left">Dashboard </span>
                    <span class="float-right"> > </span>
                </div>
            </a>
        </li>
        @if(Auth::user()->isAdmin === 1)
        <li class="nav-item mt-20" id="list-group-item" >
            <a class="nav-link " data-toggle="tab" href="#posts">
                <div class="clearfix">
                    <span class="float-left">Posts </span>
                    <span class="float-right"> > </span>
                </div>
            </a>
        </li>
        <li class="nav-item mt-20" id="list-group-item">
            <a class="nav-link " data-toggle="tab" href="#comments">
                <div class="clearfix">
                    <span class="float-left">Comments </span>
                    <span class="float-right"> > </span>
                </div>
            </a>
        </li>
        <li class="nav-item mt-20" id="list-group-item">
            <a class="nav-link " data-toggle="tab" href="#subscribers">
                <div class="clearfix">
                    <span class="float-left">Subscribers </span>
                    <span class="float-right"> > </span>
                </div>
            </a>
        </li>
        @endif
    </ul>
</aside>