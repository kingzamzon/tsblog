@extends('layouts.app')  

@section('title', ucfirst('All Categories'))

@section('content')

<section class="ptb-0">
	<div class="mb-30 brdr-ash-1 opacty-5"></div>
	<div class="container">
		<a class="mt-10" href="/"><i class="mr-5 ion-ios-home"></i>Home<i class="mlr-10 ion-chevron-right"></i></a>
		<a class="mt-10 color-ash" href="#">{{ucfirst('All Categories')}} Archive</a>
	</div><!-- container -->
</section>

<section>
<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-8">
					@include('inc.categories.world')
	                @include('inc.categories.politics')
	                @include('inc.categories.sport')
	                @include('inc.categories.business')
	                @include('inc.categories.technology')
	                @include('inc.categories.health')
	                @include('inc.categories.fashion')
	                @include('inc.categories.religion')
				</div><!-- col-md-9 -->
				
				
				  <div class="col-md-6 col-lg-4">
                     @include('inc.sidebar')
				  </div><!-- row -->
		</div><!-- container -->
</section>
@endsection