<h4 class="p-title mt-30"><b>FASHION</b></h4>

@if(count($groupedposts->where('category','Fashion')) > 0)
    <div class="row">
        @foreach ($groupedposts->where('category','Fashion')->take(6) as $post)
        <div class="col-sm-6">
        <img src="/images/{{$post->cover_image}}" alt="">
        <h4 class="pt-20"><a href="/posts/{{$post->slug}}"><b>{{$post->title}}</b></a></h4>
        <ul class="list-li-mr-20 pt-10 mb-30">
            <li class="color-lite-black">by <a href="#" class="color-black"><b>{{$post->user->name}},</b></a>
            {{date('M d, Y', strtotime($post->created_at))}}</li>
            <li><i class="color-primary mr-5 font-12 ion-chatbubbles"></i>{{count($post->comment)}}</li>
        </ul>
        </div><!-- col-sm-6 -->
        @endforeach
    </div><!-- row -->
        <a class="dplay-block btn-brdr-primary mt-20 mb-md-50" href="/category/fashion"><b>VIEW MORE FASHION</b></a>
    @else

        <div class="col-sm-12">
        No Post Found
        </div><!-- col-sm-12 -->

    @endif   
