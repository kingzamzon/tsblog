<header>
        <div class="bg-191">
            <div class="container"> 
                <div class="oflow-hidden color-ash font-9 text-sm-center ptb-sm-5">
                
                    <ul class="float-left float-sm-none list-a-plr-10 list-a-plr-sm-5 list-a-ptb-15 list-a-ptb-sm-10">
                        <li><a class="pl-0 pl-sm-10" href="#">About</a></li>
                        <li><a href="#">Advertise</a></li>
                        <li><a href="#">Submit Press Release</a></li>
                        <li><a href="#">Contact</a></li>
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li>
                                <a  href="/home" >
                                  Welcome  {{ Auth::user()->name }} 
                                </a>
                            </li>
                            <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

                            </li>
                        @endguest
                    </ul>
                    <ul class="float-right float-sm-none list-a-plr-10 list-a-plr-sm-5 list-a-ptb-15 list-a-ptb-sm-5">
                        <li><a class="pl-0 pl-sm-10" href="#"><i class="ion-social-facebook"></i></a></li>
                        <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                        <li><a href="#"><i class="ion-social-google"></i></a></li>
                        <li><a href="#"><i class="ion-social-instagram"></i></a></li>
                        <li><a href="#"><i class="ion-social-bitcoin"></i></a></li>
                    </ul>
                    
                </div><!-- top-menu -->
            </div><!-- container -->
        </div><!-- bg-191 -->

        <div class="container">
            <a class="logo" href="{{ url('/') }}">
                <img src="/images/logo-black.png" alt="Logo">
                <!-- {{ config('app.name', 'Laravel') }} -->
            </a>
            
            <a class="right-area src-btn" href="#" >
                <i class="active src-icn ion-search"></i>
                <i class="close-icn ion-close"></i>
            </a>
            <div class="src-form">
                <form role="form" method="POST" action="{{action('Post\CommentController@store')}}">
                    <input type="text" placeholder="Search here">
                    <button type="submit"><i class="ion-search"></i></a></button>
                </form>
            </div><!-- src-form -->
            
            <a class="menu-nav-icon" data-menu="#main-menu" href="#"><i class="ion-navicon"></i></a>
            
            <ul class="main-menu" id="main-menu">
                <li><a href="/category/world">NEWS</a></li>
                <li class="drop-down"><a href="/category">CATEGORY<i class="ion-arrow-down-b"></i></a>
                    <ul class="drop-down-menu drop-down-inner">
                        <li><a href="/category/world">WORLD</a></li>
                        <li><a href="/category/politics">POLITICS</a></li>
                        <li><a href="/category/sport">SPORT</a></li>
                        <li><a href="/category/business">BUSINESS</a></li>
                        <li><a href="/category/technology">TECHNOLOGY</a></li>
                        <li><a href="/category/health">HEALTH</a></li>
                        <li><a href="/category/fashion">FASHION</a></li>
                        <li><a href="/category/religion">RELIGION</a></li>
                    </ul>
                </li>
                <li><a href="/faq">FAQ</a></li>
                <li><a href="/contactus">CONTACT US</a></li>
            </ul>
            <div class="clearfix"></div>
        </div><!-- container -->
    </header>