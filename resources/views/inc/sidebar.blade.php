  <div class="pl-20 pl-md-0">
        <div class="mtb-50">
            <h4 class="p-title"><b>POPULAR POSTS</b></h4>
            @if(count($popularposts) > 0)
                @foreach ($popularposts as $popular)
                <a class="oflow-hidden pos-relative mb-20 dplay-block" 
                href="/posts/{{$popular->slug}}">
                <div class="wh-100x abs-tlr"><img src="/images/{{$popular->cover_image}}" alt=""></div>
                <div class="ml-120 min-h-100x">
                    <h5><b>{{$popular->title}}</b></h5>
                    <h6 class="color-lite-black pt-10">by <span class="color-black"><b>{{$popular->user->name}},</b></span> {{date('M d, Y', strtotime($popular->created_at))}}</h6>
                </div>
            </a><!-- oflow-hidden -->
            @endforeach
           @else 
                   <div class="ml-120 min-h-100x">
                    <h5>No Popular Posts so far</h5>
                </div>
            @endif
            
        </div><!-- mtb-50 -->
        
        <div class="mtb-50 pos-relative">
            <img src="/images/banner-1-600x450.jpg" alt="">
            <div class="abs-tblr bg-layer-7 text-center color-white">
                <div class="dplay-tbl">
                    <div class="dplay-tbl-cell">
                        <h4><b>Available for mobile & desktop</b></h4>
                        <a class="mt-15 color-primary link-brdr-btm-primary" href="#"><b>Download for free</b></a>
                    </div><!-- dplay-tbl-cell -->
                </div><!-- dplay-tbl -->
            </div><!-- abs-tblr -->
        </div><!-- mtb-50 -->
        
        <div class="mtb-50 mb-md-0">
            <h4 class="p-title"><b>NEWSLETTER</b></h4>
            <p class="mb-20">Subscribe to our newsletter to get notification about new updates,
                information, discount, etc..</p>

            <form role="form" method="POST" action="{{action('NewsletterController@store')}}" class="nwsltr-primary-1">
                {{csrf_field()}}
                <input type="text" name="email" placeholder="Your email"/>
                <button type="submit"><i class="ion-ios-paperplane"></i></button>
            </form>

        </div><!-- mtb-50 -->

</div><!--  pl-20 -->