@extends('layouts.app')

@section('content')


<div class="container-fluid">
    <div class="row">
        @include('inc.admin.sidemenu')
        <div class="col-sm-10" content-section>
          <div class="tab-content">
              <div id="dashboard" class=" tab-pane active">
                  @include('inc.admin.dashboard')
              </div>
              @if(Auth::user()->isAdmin == 1)
              <div id="posts" class="tab-pane fade">
                  @include('inc.admin.posts')
              </div>

              <div id="comments" class="tab-pane fade">
                  @include('inc.admin.comments')
              </div>

              <div id="subscribers" class="tab-pane fade">
                  @include('inc.admin.subscribers')
              </div>
              @endif
          </div>
        </div>
    </div>
</div>
@endsection
