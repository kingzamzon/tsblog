@component('mail::message')
# Hello {{$user->email}},

Thank You for subscribing to our weekly newsletter. 
You can unsubscribe with the button below.

@component('mail::button', ['url' => route('newsletter.destroy', $user->id)])
Unsubscribe
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent


