<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }} - @yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Encode+Sans+Expanded:400,600,700" rel="stylesheet">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{asset('plugin-frameworks/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('fonts/ionicons.css')}}">
    <link rel="stylesheet" href="{{asset('common/styles.css')}}">
    <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=#{property?._id}&product=custom-share-buttons"></script>
    <style type="text/css">
        .list-group {
            margin-top: 40px;
        }
        #list-group-item {
        position: relative;
        display: block;
        padding: .75rem 1.25rem;
        margin-bottom: -1px;
        border: 1px solid rgba(0,0,0,.125);
        }

        [admin-menu] {
            background-color:#000;
            height: 80vh;
            min-height: 100%;
            color: #fff;
        }
        [content-section] {
            background-color: #F1F2F7;
        }
    @media screen and  (max-width:580px) {
        [admin-menu] {
            display: none;
        }
    }
    </style>
</head>
<body>
         @include('inc.header')     
    <div class="container">
        <main class="py-4">
            
            @yield('content')
        </main>
    </div>


    @include('inc.footer')
    <!-- SCIPTS -->
    <script src="{{asset('plugin-frameworks/jquery-3.2.1.min.js')}}"></script>
    
    <script src="{{asset('plugin-frameworks/tether.min.js')}}"></script>
    
    <script src="{{asset('plugin-frameworks/bootstrap.js')}}"></script>
    
    <script src="{{asset('common/scripts.js')}}"></script>

    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('editor');
    </script>
</body>
</html>
