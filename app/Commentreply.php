<?php

namespace App;

use App\User;
use App\Comment;
use Illuminate\Database\Eloquent\Model;

class Commentreply extends Model
{
    protected $fillable = [
    	'user_id', 'comment_id', 'comment', 'isApprove'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

     public function user()
    {
    	return $this->belongsTo(Comment::class);
    }
}
