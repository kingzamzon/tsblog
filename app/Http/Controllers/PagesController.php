<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {   
        try{
            $recentposts = Post::orderBy('created_at','desc')->get()->take(5);
            $groupedposts = Post::orderBy('created_at','desc')->paginate(20);
            $popularposts = Post::orderBy('views','desc')->get()->take(5);
        }catch(Exception $e){

        }
       
    	return view('pages.index')->with('recentposts', $recentposts)->with('popularposts', $popularposts)->with('groupedposts', $groupedposts);
    }

    public function allCategory()
    {
        $popularposts = Post::orderBy('views','desc')->get()->take(5);
        $groupedposts = Post::orderBy('created_at','desc')->get();
        return view('inc.categories.allcategory')->with('popularposts', $popularposts)->with('groupedposts', $groupedposts);
    }

    public function category($type)
    {
        $posts = Post::orderBy('created_at','desc')->whereCategory($type)->paginate(10);
        $popularposts = Post::orderBy('views','desc')->get()->take(5);
    	return view('pages.category')->with('posts', $posts)->with('type',$type)->with('popularposts', $popularposts);
    }

    public function faq()
    {
    	return view('pages.faq');
    }


}
