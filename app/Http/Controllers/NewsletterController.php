<?php

namespace App\Http\Controllers;

use App\Newsletter;
use App\Mail\UserSubscribe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class NewsletterController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'email' => 'required|string|email|unique:newsletters'
        ];

        $this->validate($request, $rules);
        $data = $request->all();

        $subscriber = Newsletter::create($data);
        
         retry(5, function() use ($subscriber){
               Mail::to($subscriber)->send(new UserSubscribe($subscriber));
        }, 100);

     

        return redirect('/')->with('success', 'subscription Successful');;
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subscriber = Newsletter::find($id);
        $subscriber->delete();
        return redirect('/')->with('success', 'Unsubscribe Successful');
    }
}
