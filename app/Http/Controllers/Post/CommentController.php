<?php

namespace App\Http\Controllers\Post;

use App\Post;
use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['destroy'] ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|string|email',
            'post_id' => 'required',
            'comment' => 'required|string'
        ];

        $this->validate($request, $rules);

        $data = $request->all();

        $data['isApprove'] = 0;
        $data['favourite'] = 0;

        $comment = Comment::create($data);
        $post = Post::where('id', $comment->post_id)->first();


        return redirect()->route('posts.show', ['slug' => $post->slug])->with('success', 'Comment Waiting For Approval');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Comment::find($id);
        $comment->favourite = $comment->favourite+1;
        $comment->save();
        return redirect()->route('posts.show', ['id' => $comment->post_id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comment = Comment::find($id);
        $comment->isApprove = 1;
        $comment->save();
        return redirect('/home')->with('success', 'Comment Approved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::find($id);
        if(auth()->user()->isAdmin == 1){
            $comment->delete();
            return redirect('/home')->with('success', 'Comment Removed');
        };
         return redirect('/home')->with('error', 'Unauthorized Page');
    }
}
