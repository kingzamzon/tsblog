<?php

namespace App\Http\Controllers\Post;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show'] ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('created_at','desc')->paginate(10);
        return view('pages.index')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->user()->isAdmin !== 1){
            return redirect('/')->with('error', 'Unauthorized Page');
        };
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image|required',
            'category' => 'required'
        ];

        $this->validate($request, $rules);

        $data = $request->all();

        $data['title'] = $request->title;
        $data['body'] = $request->body;
        $data['slug'] = str::slug($request->title, '-');
        $data['category'] = $request->category;
        $data['cover_image'] = $request->cover_image->store('');
        $data['user_id'] = auth()->user()->id;
        $data['views'] = 1;

        $post = Post::create($data);


        return redirect('/posts/create')->with('success', 'Post created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Post::where('slug', $slug)->first();

        $post->views += 1;
        $post->save();
        $randomposts = Post::whereCategory($post->category)->get()->random(1);
        $popularposts = Post::orderBy('views','desc')->get()->take(5);
        return view('posts.show')->with('post', $post)->with('randomposts', $randomposts)->with('popularposts', $popularposts);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $post = Post::where('slug', $slug)->first();

        //check for correct user
        if(auth()->user()->id !== $post->user_id){
            return redirect('/posts')->with('error', 'Unauthorized Page');
        };
        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);

        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'category' => 'required',
            'cover_image' => 'image'
        ]);


        
        if($request->has('title')){
            $post->title = $request->title;
            $post->slug = str::slug($request->title, '-');
        }

        if($request->has('body')) {
            $post->body = $request->body;
        }
        
        if($request->has('category')) {
            $post->category = $request->category;
        }
        
        if($request->hasFile('cover_image')) {
            $post->cover_image = $request->cover_image->store('');
        }
        $post->save();
        return redirect()->route('posts.edit', ['slug' => $post->slug])->with('success', 'Post Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        if(auth()->user()->isAdmin !== 1){
            return redirect('/home')->with('error', 'Unauthorized Page');
        };
        Storage::delete($post->cover_image);
        $post->delete();
        return redirect('/home')->with('success', 'Post Removed');
    }
}
