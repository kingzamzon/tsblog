<?php

namespace App\Http\Controllers;

use App\Post;
use App\Comment;
use App\Newsletter;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = Post::orderBy('created_at','desc')->paginate(20);
        $subscribers = Newsletter::orderBy('created_at','desc')->paginate(20);
        $unapprovedcomments = Comment::orderBy('created_at','desc')->paginate(20);
        return view('home')->with('posts', $posts)->with('subscribers', $subscribers)->with('unapprovedcomments',$unapprovedcomments);
    }
}
