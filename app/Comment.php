<?php

namespace App;

use App\User;
use App\Post;
use App\Commentreply;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
    	'name', 'email', 'post_id', 'comment', 'isApprove','favourite'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function post()
    {
    	return $this->belongsTo(Post::class);
    }

    public function commentreply()
    {
        return $this->hasMany(Commentreply::class);
    }
}
