<?php

namespace App;

use App\User;
use App\Comment;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $table = 'posts';

    protected $primaryKey = 'id';

    protected $fillable = [
    	'user_id', 'title', 'slug' ,'body', 'cover_image', 'category', 'views'
    ];
    protected $casts = [
        'created_at' => 'datetime:M d, Y'
    ];
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function comment()
    {
    	return $this->hasMany(Comment::class);
    }
}
