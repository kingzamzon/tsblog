<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');

Route::get('/category', 'PagesController@allCategory');

Route::get('/category/{type}', 'PagesController@category');

Route::get('/faq', 'PagesController@faq');

Route::resource('posts', 'Post\PostController');

Route::get('/posts/{slug}','Post\PostController@show')->name('posts.show');

Route::get('/posts/{slug}/edit','Post\PostController@edit')->name('posts.edit');

Route::resource('comments', 'Post\CommentController');

Route::resource('newsletter', 'NewsletterController', ['only'=>['store','destroy']]);

Route::resource('contactus', 'ContactUsController');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
